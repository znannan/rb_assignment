# -*- coding: UTF-8 -*-

#1.Write a method call array_uppercase. It accepts an non empty array as
#parameter
#Input: array contains string elements
#Output: All the characters of array elements should be upcase.
#Example: input ["hello", "world"] , output ["HELLO", "WORLD"]
def array_uppercase (arr)
    if arr == []
        puts "array should not be empty!"
    else
        arr_output = []
        arr.each {|word| arr_output.push(word.upcase) }
        return arr_output
    end
    
end

arr1 = ['hello', 'world']
puts "=========1=========="
puts(array_uppercase(arr1))

#2. Write a method call sum, it accepts an non empty array contains all integer
#elements.
#Input: non empty array contains all integer
#Output: The sum of all elements of the array
#Example: input [1,2,3,4,5,6], output 21

def sum (arr)
    if arr == []
        puts "array should not be empty!"
    else
        sum_num = 0
        arr.each {|a| is_int = a.is_a?(Integer)
            if is_int == false
                puts "each array elements should be a Integer!"
                exit
            elsif is_int == true
                sum_num += a
            end
        }
        return sum_num
    end
end

arr2 = [1,2,3,4,5,6]
puts "=========2=========="
puts(sum(arr2))

# 3. Write a class called Student
# a)  its instance has four attributes: name, age, program, major, These
# attributes are read only from outside.
# b) The constructor is the only place we can set name, age, program and major.
# c)  Write a display method to display the information of the student in the
# format "#{name}-#{age}-#{program}-#{major}"

class Student
    attr_reader :name, :age, :program, :major
    def initialize(name,age,program,major)
        @name, @age, @program, @major = name,age,program,major
    end

    def display (name=@name,age=@age,program=@program,major=@major)
        puts "#{name}-#{age}-#{program}-#{major}"
    end
end

stu1 = Student.new("John",21,"MA","IT")
puts "=========3=========="
stu1.display

# 4. Write a method call sort_students
# Input: a non-empty array whose elements are all instances of Student class, and
# one of the four attributes symbols: :name, :age, :program, :major
# Output: ascend sorted students array by the attributes.
# Example:
# student_array = [
# Student.new("John", 21, "MA", "IT"),
# Student.new("Ablaham", 22, "BA", "Design"),
# Student.new("Noel", 20, "MDIV", "Theology"),
# Student.new("Peter", 23, "PHD", "Ecology"),
# ]
# sort_students(student_array, :age) should produce the following result:
# [
# Student.new("Noel", 20, "MDIV", "Theology"),
# Student.new("John", 21, "MA", "IT"),
# Student.new("Ablaham", 22, "BA", "Design"),
# Student.new("Peter", 23, "PHD", "Ecology"),

def sort_students(student_array, attr_name)
    output_arr = []
    attr_name = attr_name.to_s
    if attr_name=="age"
        output_arr = student_array.sort_by {|a| a.age}
    elsif attr_name=="name"
        output_arr = student_array.sort_by {|a| a.name}
    elsif attr_name=="program"
        output_arr = student_array.sort_by {|a| a.program}
    elsif attr_name=="major"
        output_arr = student_array.sort_by {|a| a.major}
    end
    return output_arr
end

student_array = [
    Student.new("John", 21, "MA", "IT"),
    Student.new("Ablaham", 22, "BA", "Design"),
    Student.new("Noel", 20, "MDIV", "Theology"),
    Student.new("Peter", 23, "PHD", "Ecology"),
]
puts "=========4=========="
puts "-------input-------"
student_array.each{|s| s.display}
puts "----output sorted----"
(sort_students(student_array, :name)).each{|s| s.display}